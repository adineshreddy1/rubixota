
from utils import util
from chalice import Response

import json

def main(thing):
    """ Executes the published job on the thing"""

    try:
        output = util.execute_command(thing)
        summary = json.loads(output.splitlines()[-1].strip().strip('Stats: '))
        print ("Summary : ",summary)

        jobsStarted = summary.get("jobsStarted")
        jobsSucceeded = summary.get("jobsSucceeded")
        jobsRejected = summary.get("jobsSucceeded")


        if jobsStarted == jobsSucceeded and jobsRejected == 0:
            return Response(
                status_code=200,
                body= summary
                )
        else:
            return Response(
                status_code=500,
                body = summary
            )

    except Exception as error:
            return Response(
                status_code=500,
                body='Error: {}'.format(error)
            )