"""retrieves full list of all devices: unregistered, registered, and provisioned
Returns:
    List -- list of devices
"""

from boto3.dynamodb.conditions import Key, Attr
import boto3
from chalice import Response

DEVICE_TABLE = boto3.resource('dynamodb').Table('cdf-commands-jobs-development')


def main():
    """returns list of registered hardware devices
    Returns:
        Array -- returns list of registered hardware devices and device property data
    """
    try:
        result = DEVICE_TABLE.scan(
            FilterExpression = Attr('commandStatus').eq('PUBLISHED')
            )

        return result['Items']
    except Exception as e:
        return Response(
            body = {
            	'message': 'Error: {}'.format(e)
            },
            status_code = 400
        )
