"""Create OTA job via CDF commands via CDF Commands Library"""
import os
import json
import requests
from chalice import Response, BadRequestError, NotFoundError, ChaliceViewError

COMMANDS_URL = os.getenv('COMMANDS_URL')
# COMMANDS_URL ="https://1rlt5nb45h.execute-api.us-east-1.amazonaws.com/Prod"
BASEPATH = COMMANDS_URL + '/commands'
BASEPATH_template = COMMANDS_URL + '/templates'
HEADERS = {
  'Accept': 'application/vnd.aws-cdf-v1.0+json',
  'Content-Type': 'application/vnd.aws-cdf-v1.0+json'
}

def cdf_post_request(payload):
    """POST request for CDF Commands"""
    try:
        cdf_temp_response = requests.post(BASEPATH_template, headers=HEADERS, data=json.dumps(payload))
        if cdf_temp_response.status_code == 204:
            cdf_response = requests.post(BASEPATH, headers=HEADERS, data=json.dumps(payload))
            return cdf_response
        else:
            return cdf_temp_response
    except Exception as err:
        raise ChaliceViewError(err)

def create_job_draft(filter_data,flag):
    """Receive payload and map -> cdf commands targetQuery"""
    # Creating ("Entering create_job_draft")

    if flag == 'multi':
        #Yet to be implemented for multiple devices
        print("multi")
    else:
        # OTA for a single device
        commands_template = {

            "templateId": filter_data['templateId'],
           "operation":filter_data['operation'],
            "document":filter_data['document'],
            "protocols": ["MQTT"],
            "roles-arn": filter_data['roles-arn'],
            "targets": filter_data['targets'],
            "type": filter_data['type'],
            "description": filter_data['description'],
            "rolloutMaximumPerMinute":filter_data['rolloutMaximumPerMinute'],
            "presignedUrlExpiresInSeconds": filter_data['presignedUrlExpiresInSeconds']

            }

        return cdf_post_request(commands_template)

def publish_job(command_id):
    # logger.info("Entering publish_job")
    url = BASEPATH + '/' + command_id

    payload = {
      'commandStatus': 'PUBLISHED'
    }
    try:
        cdf_response = requests.patch(url, headers=HEADERS, data=json.dumps(payload))
        return cdf_response.status_code
    except Exception as err:
        raise ChaliceViewError(err)


def main(filter_data, flag):
    """Use incoming filter data to create ota job draft
    and publish it. Return the job_id for further API requests."""

    job_draft_response = create_job_draft(filter_data,flag)
    if job_draft_response.status_code == 204:
        command_id = job_draft_response.headers['location'].split("/")[-1]
        publish_response = publish_job(command_id)
        if publish_response == 204:
            return Response(
                status_code = 200,
                body = { "message":"Job published successfully",
                         "job id":'cdf-' + command_id
                        }
            )
        else:
            raise ChaliceViewError("CDF Job publish error")
    else:
        raise ChaliceViewError(job_draft_response.text)
