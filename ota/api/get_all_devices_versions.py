"""retrieves full list of all devices: unregistered, registered, and provisioned
Returns:
    List -- list of devices
"""
import os
import boto3
from chalice import Response

DEVICE_TABLE = boto3.resource('dynamodb').Table(os.getenv('DEVICE_TABLE'))

def main():
    """retrieves list of all devices version in table
    Returns:
        List -- list of devices version info
    """
    try:
        version_list = []
        response = DEVICE_TABLE.scan()
        for item in response['Items']:

                resp = {
                    "id": item['id'],
                    "ver_info": item['ver_info']
                }
                version_list.append(resp)
        return Response(
            status_code=200,
            body=version_list
        )
    except Exception as error:
        return Response(
            status_code=400,
            body='Error: {}'.format(error)
        )
