
import os
import boto3
from chalice import Response

DEVICE_TABLE = boto3.resource('dynamodb').Table(os.getenv('DEVICE_TABLE'))

def main():
    """retrieves list of all devices in table
    Returns:
        List -- list of devices
    """
    try:
        response = DEVICE_TABLE.scan()

        return Response(
            status_code=200,
            body=response['Items']
        )
    except Exception as error:
        return Response(
            status_code=400,
            body='Error: {}'.format(error)
        )
