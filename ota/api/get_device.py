"""retrieve a device details and pre-signed url for keys and certificates
Returns:
    Dict -- contains device records and pre-signed url
"""
import logging
import os
import boto3
from chalice import Chalice, Response


DEVICE_TABLE = boto3.resource('dynamodb').Table(os.getenv('DEVICE_TABLE'))

s3 = boto3.client('s3')


def main(req):
    """retrieve a single devices details and pre-signed url for
    associated certificates download
    Arguments:
        req {Object} -- contains id of the device to be retrieved
    Returns:
        [Object] -- contains details of device and pre-signed url
        for associated certificates
    """
    try:

        item = DEVICE_TABLE.get_item(
            Key={
                'id': req['id']
            }
        )['Item']
        # url = s3.generate_presigned_url(
        #     'get_object',
        #     Params = {
        #         'Bucket': 'rubixdemo-app-storage',
        #         'Key': 'certificates/{}/certs.zip'.format(req['id'])
        #     },
        #      ExpiresIn = 3600
        # )
        response = {
            "item": item
            # "url": url
        }

        return Response(
            status_code=200,
            headers={
                'Content-Type': 'application/json'
            },
            body=response)
    except Exception as e:
        print('>>> FAILED TO GET ITEM', e)
        body = {
                   'message': 'Error: {}'.format(e)
               }
        print (body)
        return Response(
			body = {
				'message': 'Error: {}'.format(e)
			},
			status_code = 400
		
		)
