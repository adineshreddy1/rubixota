import boto3
from botocore.exceptions import ClientError

def upload_file(req):
    s3_client = boto3.client('s3')
    # Upload the file
    try:
        response = s3_client.upload_file(req['file_name'],req['bucket'],req['object_name'])
        return response
    except ClientError as e:
        return False

