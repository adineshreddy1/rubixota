from chalice import Chalice, Response

from api import  (
get_device,
get_all_devices,
get_all_devices_versions,
get_all_jobs,
get_device_list,
uploadfile,
get_publishedjobs,
create_ota_job,
execute_ota_job
)

app = Chalice(app_name='ota')

# #  get list of devicesgetbycommanid
@app.route('/getalldevices', cors=True, methods=['GET'])
def route_get_all_devices():
    """retrieves list of all devices in table
    Returns:
        List -- list of devices
    """
    return get_all_devices.main()
# get device details
@app.route('/getdevice', cors=True, methods=['POST'])
def route_get_device():
    """retrieve a single devices details and pre-signed url for
    associated certificates download
    Arguments:
        req {Object} -- contains id of the device to be retrieved
    Returns:
        [Object] -- contains details of device and pre-signed url
        for associated certificates
    """
    req = app.current_request.json_body
    return get_device.main(req)
# get devicelist details
@app.route('/getdevicelist', cors=True, methods=['POST'])
def route_get_device_list():
    """retrieve a multiple devices details and pre-signed url for
    associated certificates download
    Arguments:
        req {Object} -- contains ids of the devices to be retrieved
    Returns:
        [Object] -- contains details of device and pre-signed url
        for associated certificates
    """
    req = app.current_request.json_body
    return get_device_list.main(req)
# #  get list of devices versions
@app.route('/getalldeviceVersions', cors=True, methods=['GET'])
def route_get_all_devices_versions():
    """retrieves list of all devices versions in table
    Returns:
        List -- list of devices versions
    """
    return get_all_devices_versions.main()
@app.route('/listalljobs', cors=True, methods=['GET'])
def route_get_all_jobs():
    """retrieves list of all devices in table
    Returns:
        List -- list of devices
    """
    return get_all_jobs.main()

@app.route('/uploadfile', cors=True, methods=['PUT'])
def route_uploadfile():
    """retrieves list of all devices in table
    Returns:
        List -- list of devices
    """
    req = app.current_request.json_body
    return uploadfile.upload_file(req)

@app.route('/getpublishedjobs', cors=True, methods=['GET'])
def route_get_publishedjobs():
    """retrieves list of all devices versions in table
    Returns:
        List -- list of devices versions
    """
    return get_publishedjobs.main()

@app.route('/create_ota_job', cors=True, methods=['POST'])
def route_create_ota_job():
    """retrieves list of all devices in table
    Returns:
        List -- list of devices
    """
    req = app.current_request.json_body
    return create_ota_job.main(req,'single')

@app.route('/execute_job', cors=True, methods=['POST'])
def route_execute_job():
    """retrieves list of all devices in table
    Returns:
        List -- list of devices
    """
    req = app.current_request.json_body
    execute_ota_job.main(req)
