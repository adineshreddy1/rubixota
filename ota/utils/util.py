import  os
import boto3
import requests
import subprocess

def get_iot_endpoint():
    """ Get the IoT endpoint address"""
    iot_client = boto3.client('iot')
    iot_endpoint = iot_client.describe_endpoint(endpointType="iot:Data-ATS")['endpointAddress']
    return iot_endpoint

def get_root_ca():
    """ Gets the root-CA certificate from Amazon Repository"""
    cert = requests.get("https://www.amazontrust.com/repository/AmazonRootCA1.pem")
    with open('root-ca.pem','w') as fd:
        fd.write(cert.text)

def execute_command(thing):
    """ Executes the job command on the given thing using AWSIoTPythonSDK """
    command = "python " + os.getcwd() + "\\utils\jobsSample.py -e " + get_iot_endpoint() + " -r root-ca.pem -w -n " + thing['thing']
    # print("Execution Command :\n", command)
    output = subprocess.Popen(command, stdout=subprocess.PIPE, universal_newlines=True).communicate()[0]
    return output

